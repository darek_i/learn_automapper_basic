﻿using AutoMapper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleApp1
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            // Create configuration with one mapping and no profile
            MapperConfiguration config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<InputClass, OutputClass>()
                .ForMember(dest => dest.C, opt => opt.MapFrom( src => src.A + src.B));
            });
            // Instanciate mappper
            IMapper mapper = config.CreateMapper();

            InputClass input = new InputClass()
            {
                A = 1,
                B = 2
            };


            // Mapping to OutputClass
            var output = mapper.Map<OutputClass>(input);



            #region Display
            // Print input object
            Console.WriteLine("Input:");
            Console.WriteLine(JsonConvert.SerializeObject(input));
            Console.WriteLine("");

            // Print output object
            Console.WriteLine("Output:");
            Console.WriteLine(JsonConvert.SerializeObject(output));
            Console.WriteLine("");

            // Keep console from closing
            Console.ReadLine();
            #endregion
        }
    }
}
